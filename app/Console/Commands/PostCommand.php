<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Posts\Models\Post;

class PostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new a post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Post::create([
            'name' => 'Post command',
            'slug' => 'post-comand',
            'content' => 'test',
            'type' => 1,
        ]);
    }
}
