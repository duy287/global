<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('created_at', 'desc')->paginate(2);
        return response()->json($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $category = new Category();
        $category->name = $request->name;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->storeAs(
                'public/categories', $file->getClientOriginalName()
            );
            $category->image = $file->getClientOriginalName();
        }

        if($category->save()){
            return response()->json($category, 200);
        }
        else{
            return response()->json([
                'message'=> 'Some error accurred, please try again',
                'status_code'=> 500
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|min:3',
        ]);
        $category->name = $request->name;

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg'
            ]);

            $file = $request->file('image');
            //Delete old file
            if (Storage::disk('public')->exists('categories/', $category->image)) {
                Storage::disk('public')->delete("categories/$category->image");
            }
            $path = $file->storeAs(
                'public/categories', $file->getClientOriginalName()
            );
            $category->image = $file->getClientOriginalName();
        }

        if($category->save()){
            return response()->json($category, 200);
        }
        else{
            return response()->json([
                'message'=> 'Some error accurred, please try again',
                'status_code'=> 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->delete()){
            //delete image
            Storage::disk('public')->delete("categories/$category->image");
            return response()->json([
                'message' => 'Category deleted successfully',
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'message' => 'Some error occured, please try again',
                'status_code' => 500
            ], 500);
        }
    }
}
