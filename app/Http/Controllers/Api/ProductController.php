<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(2);
        return response()->json($products, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'category_id'=> 'required'
        ]);
        $product = new Product();
        $product->name = $request->name;
        $product->category_id = $request->category_id;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->storeAs(
                'public/products', $file->getClientOriginalName()
            );
            $product->image = $file->getClientOriginalName();
        }

        if($product->save()){
            return response()->json($product, 200);
        }
        else{
            return response()->json([
                'message'=> 'Some error accurred, please try again',
                'status_code'=> 500
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required|min:3',
            'category_id' => 'required'
        ]);
        $product->name = $request->name;
        $product->category_id = $request->category_id;

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg'
            ]);

            $file = $request->file('image');
            //Delete old file
            if (Storage::disk('public')->exists('products/', $product->image)) {
                Storage::disk('public')->delete("products/$product->image");
            }
            $path = $file->storeAs(
                'public/products', $file->getClientOriginalName()
            );
            $product->image = $file->getClientOriginalName();
        }

        if($product->save()){
            return response()->json($product, 200);
        }
        else{
            return response()->json([
                'message'=> 'Some error accurred, please try again',
                'status_code'=> 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->delete()){
            //delete image
            Storage::disk('public')->delete("products/$product->image");
            return response()->json([
                'message' => 'Product deleted successfully',
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'message' => 'Some error occured, please try again',
                'status_code' => 500
            ], 500);
        }
    }

    public function categories()
    {
        $categories = Category::all();
        return response()->json($categories, 200);
    }
}
