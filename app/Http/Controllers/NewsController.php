<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Repositories\Contracts\INews;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    protected $newsRepository;

    public function __construct(INews $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->newsRepository->datatable();
        }
        return view('news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $groups = $this->newsRepository->create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'url' =>  Str::slug($request->title, '-'),
            'image' => $request->image
        ]);
        return redirect()->route('news.index')->with([
            'flash_level'   => 'success',
            'flash_message' => 'Add success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = $this->newsRepository->find($id);
        return view('news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->newsRepository->update($id,[
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'url' =>  Str::slug($request->slug, '-')
        ]);
        return redirect()->route('news.index')->with([
            'flash_level'   => 'success',
            'flash_message' => 'Update success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->newsRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => 'News deleted successfully',
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => 'Some error occured, please try again',
                'status_code' => 500
            ], 500);
        }
    }
}
