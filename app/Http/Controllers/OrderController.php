<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
use App\Order;
use App\User;
use App\Jobs\SendReminderEmail;
use App\Notifications\InvoicePaid;
use App\Notifications\OrderComplete;

class OrderController extends Controller
{
    /**
     * Ship the given order.
     *
     * @param   Request  $request
     * @param   int  $orderId
     * @return  Response
     */
    public function ship($orderId)
    {
        $order = Order::findOrFail($orderId);
        $user =  User::find(1);
        
        // Ship order...
        Mail::to($user)->send(new OrderShipped($order));
        return response('Send mail success');
    }

    public function queueShip($orderId)
    {
        $order = Order::findOrFail($orderId);
        SendReminderEmail::dispatch($order, request()->user());
        return response('Send mail success');
    }

    public function sendNotification($orderId)
    {
        $order = Order::findOrFail($orderId);
        $user =  User::find(1);
        
        $when = now()->addMinutes(1);
        $user->notify((new InvoicePaid($order, $user))->delay($when));
        return response('send notification success');
    }

    public function sendDatabaseNotification($orderId){
        $order = Order::findOrFail($orderId);
        $user =  User::find(1);
        //send notification
        $user->notify(new OrderComplete($order));

        return view('pages.notification', compact('user'));
    }

    public function markNotification($user_id){
        $user =  User::find(1);
        $user->unreadNotifications->markAsRead();
        return response('mark notification success');
    }
}
