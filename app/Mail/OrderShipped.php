<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Order;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The object instance.
     */
    protected $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.shipped')
        ->with([
            'order' => $this->order,
            'img_path' => Storage::disk('public')->path('banners/img_snowtops.jpg')
        ]);
    }
}
