<?php
namespace App\Repositories\Eloquent;
use App\Exceptions\ModelNotDefined;
use App\Repositories\Contracts\IBase;
// use App\Repositories\Criteria\ICriteria;
use Illuminate\Support\Arr;

abstract class BaseRepository implements IBase {
    protected  $model;
    public function __construct()
    {
        $this->model= $this->getModelClass();
    }

    protected function getModelClass(){
        if(!method_exists($this,'model')){
            throw new ModelNotDefined ();
        }
        return app()->make($this->model()); // trả về một model object
    }

    public function all(){
        return $this->model->get();
    }

    function datatable(){
        $query=$this->model->get();
        return datatables($query)
        ->addColumn('edit_url', function ($item) {
            return route('news.edit', $item->id);
        })
        ->make(true);
    }

    public function find($id){
        return $this->model->findOrFail($id);
    }

    public function findWhere($column,$value){
        return $this->model->where($column,$value)->get();
    }

    public function finWhereFirst($column,$value){
        return $this->model->where($column,$value)->firstOrFail();
    }

    public function where($column,$value){
        return $this->model->where($column,$value);
    }

    public function orWhere($column, $value){
        return $this->model->orWhere($column,$value);
    }

    /**
     * Retrieve all data of repository, paginated
     * @param int $perPage
     */
    public function pagination($perPage=10){
        return $this->model->paginate($perPage);
    }

    public function create(array $data){
        return $this->model->create($data);
    }

    public function update($id, array $data){
        $record = $this->find($id);
        return $record->update($data);
    }

    public function delete($id){
        $record = $this->find($id);
        return $record->delete();
    }
}
