<?php
namespace App\Repositories\Eloquent;
use App\News;
use App\Repositories\Contracts\INews;
use Illuminate\Support\Facades\Storage;

class NewsRepository extends BaseRepository implements INews {

    public function model(){
        return News::class ;
    }

    public function create(array $data)
    {
        if ($data['image']) {
            $file = $data['image'];
            if($file->store('public/news')){
                $data['image'] = $file->getClientOriginalName();
            }
        }
        return $this->model->create($data);
    }
}
