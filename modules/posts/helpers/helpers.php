<?php 
use Carbon\Carbon;
if (!function_exists('table_actions')) {
    /**
     * @param $edit
     * @param $delete
     * @return \Illuminate\View\View
     */
    function table_actions($edit, $delete, $item)
    {
        return view('items.action', compact('edit', 'delete', 'item'))->render();
    }
}
if (!function_exists('table_status')) {
    /**
     * @param int $status
     * @return \Illuminate\View\View
     */
    function table_status($status, $active_link)
    {
        $badge = [];
        switch($status){
            case 0: 
                $badge = [
                    'class' => 'badge-secondary',
                    'text' => trans('action.no'),
                    'link' => $active_link
                ]; 
                break;
            case 1: 
                $badge = [
                    'class' => 'badge-success',
                    'text' => trans('action.yes'),
                    'link' => $active_link
                ]; 
                break;
            default: 
                $badge = [
                    'class' => 'badge-secondary',
                    'text' => trans('action.no'),
                    'link' => $active_link
                ]; 
        }
        return view('items.status', compact('badge'))->render();
    }
}
if (!function_exists('format_date')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function format_date($time, $format = 'Y-m-d')
    {
        if (empty($time)) {
            return "";
        }
        return Carbon::parse($time)->format($format);
    }
}