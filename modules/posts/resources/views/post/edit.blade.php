@extends('layout.app')

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Form</div>
            <div class="card-body">
                <form method="POST" action="{{ route('posts.post_edit', $post->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group @error('name') has-danger @enderror">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name', $post->name) }}" class="form-control" id="name" placeholder="name">
                        @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group @error('slug') has-danger @enderror">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" value="{{ old('slug', $post->slug) }}" onfocusout="convertToSlug($(this))" class="form-control" id="slug" placeholder="slug">
                        @error('slug')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" name="content" id="content" rows="3">{{ old('content', $post->content) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="banner">Banner</label>
                        <input type="file" name="banner" id="banner">
                    </div>
                    <div class="form-group @error('type') has-danger @enderror">
                        <label for="type">Type</label>
                        <select class="form-control" name="type">
                            <option value="" disabled>Choose type...</option>
                            <option value="1" {{ old('type', $post->type) == 1 ? 'selected' : '' }}>Normal</option>
                            <option value="2" {{ old('type', $post->type) == 2 ? 'selected' : '' }}>Hot news</option>
                        </select>
                        @error('type')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@push('page-scripts')
<script>
function convertToSlug(ele)
{
    let text = ele.val().toLowerCase();
    text = text.replace(/[^\w- ]+/g,'').replace(/ +/g,'-');
    ele.val(text);
}
</script>
@endpush