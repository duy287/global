@extends('layout.app')

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Example</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Content</th>
                                <th>Created at</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Confirm dialog-->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <strong>{{__('action.delete_confirm')}}</strong>
            </div>
            <div id="confirmMessage" class="modal-body">
                {{__('action.delete_sure')}}
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmCancel" class="btn btn-default btn-cancel"
                        data-dismiss="modal">
                    {{__('action.cancel')}}
                </button>
                <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteItem($(this).val())">
                    {{__('action.delete')}}
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('posts.data') }}",
        columns: [
            { data: 'name', name: 'name' },
            { data: 'content', name: 'content' },
            { data: 'created_at', name: 'created_at', searchable: false},
            { data: 'status', name: 'status', orderable: false, searchable: false},
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    function deleteItem(url) {
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                "_token" : "{{ csrf_token() }}",
            },
            success: (data) => {
                if(data.status)
                    location.reload();
                else 
                    alert(data.message);
            },
        });
    }
</script>
@endpush