<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

Route::get('/posts-welcome', function(){
    return View("Posts::welcome");
});

Route::group(['namespace' => 'Modules\Posts\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/', [
            'as' => 'posts.index',
            'uses' => 'PostController@getIndex',
        ]);
        Route::get('/add', [
            'as' => 'posts.add',
            'uses' => 'PostController@getAdd',
        ]);
        Route::post('/post-add', [
            'as' => 'posts.post_add',
            'uses' => 'PostController@postAdd',
        ]);
        Route::get('/edit/{id}', [
            'as' => 'posts.edit',
            'uses' => 'PostController@getEdit',
        ]);
        Route::post('/edit/{id}', [
            'as' => 'posts.post_edit',
            'uses' => 'PostController@postEdit',
        ]);
        Route::post('/delete/{id}', [
            'as' => 'posts.delete',
            'uses' => 'PostController@postDelete',
        ]);
        Route::post('/status/{id}', [
            'as' => 'posts.status',
            'uses' => 'PostController@postStatus',
        ]);
        Route::group(['prefix' => 'api'], function () {
            Route::get('/data', [
                'as' => 'posts.data',
                'uses' => 'ApiController@getPosts',
            ]);
        });
    });
});