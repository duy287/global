<?php 

namespace Modules\Posts\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Posts\Models\Post;
use Modules\Posts\Repositories\Interfaces\PostInterface;

class ApiController extends Controller
{
    protected $postRepository;

    public function __construct(PostInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getPosts()
    {
        return $this->postRepository->getDataTable();
    }

}