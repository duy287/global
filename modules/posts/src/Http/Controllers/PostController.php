<?php 

namespace Modules\Posts\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Posts\Models\Post;
use Modules\Posts\Repositories\Interfaces\PostInterface;
use Illuminate\Http\Request;
use Modules\Posts\Http\Requests\PostRequest;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getIndex(){
        return view('Posts::post.index');
    }

    public function getAdd(){
        return view('Posts::post.add');
    }

    public function postAdd(PostRequest $request){
        $post = new Post();
        $this->postRepository->save($request, $post);
        return redirect()->route('posts.index')->with([
            'flash_level' => 'success',
            'flash_message' => trans('action.success')
        ]);
    }

    public function getEdit($id){
        $post = $this->postRepository->find($id);
        return view('Posts::post.edit', compact('post'));
    }

    public function postEdit($id, PostRequest $request){
        $post = $this->postRepository->find($id);
        $this->postRepository->save($request, $post);
        return redirect()->route('posts.index')->with([
            'flash_level' => 'success',
            'flash_message' => trans('action.success')
        ]);
    }

    public function postDelete($id){
        $check = $this->postRepository->delete($id);
        if($check){
            return response()->json([
                'status' => 1
            ]);
        }
        return response()->json([
            'status' => 0,
            'message' => 'Delete error'
        ]);
    }

    public function postStatus($id){
        $status = $this->postRepository->changeStatus($id);
        return response()->json([
            'status' => $status
        ]);
    }


}