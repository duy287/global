<?php

namespace Modules\Posts\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        if($id){
            $rules = [
                'name' =>[
                    'required',
                    Rule::unique('posts')->ignore($id)
                ],
                'slug' => 'required',
                'type' => 'required'
            ];
        }
        else{
            $rules = [
                'name' =>'required|unique:posts,name',
                'slug' => 'required',
                'type' => 'required'
            ];
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name'=> 'Name',
            //...
        ];
    }
}
