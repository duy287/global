<?php

namespace Modules\Posts\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{ 
    protected $fillable = ['name', 'content', 'slug', 'type', 'banner'];
}
