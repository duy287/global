<?php 

namespace Modules\Posts\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Posts\Repositories\Eloquents\PostRepository;
use Modules\Posts\Repositories\Interfaces\PostInterface;
use File;

class MainServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'Posts');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'Posts');
    }

    public function register()
    {
        // Merge config
        $this->mergeConfigFrom(__DIR__ . '/../../config/post_ml.php', 'post_ml');

        // Add Provider for service container
        $this->app->singleton(PostInterface::class, function () {
            return new PostRepository();
        });

        // Load helper
        $this->autoload(__DIR__ . '/../../helpers');
    }

    public static function autoload($directory)
    {
        $helpers = File::glob($directory . '/*.php'); //get all files in directory
        // load file into App
        foreach ($helpers as $helper) {
            File::requireOnce($helper);
        }
    }
}