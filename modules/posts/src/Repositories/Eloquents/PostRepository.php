<?php 
namespace Modules\Posts\Repositories\Eloquents;

use Modules\Posts\Repositories\Interfaces\PostInterface;
use Modules\Posts\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PostRepository implements PostInterface
{
    /**
     * Return dataTables
     * @return mixed 
     */
    public function getDataTable(){
        return datatables(Post::query()) 
        ->addColumn('action', function ($item) {
            return table_actions('posts.edit', 'posts.delete', $item);
        })
        ->editColumn('status', function ($item) {
            return table_status($item->status, route('posts.status', $item->id));
        })
        ->editColumn('created_at', function ($item) {
            return format_date($item->created_at);
        })
        ->rawColumns(['status', 'action']) //chỉ định các cột muốn hiển thị dạng HTML
        ->make(true);
    }

    /**
     * Find one post
     * @param $id
     * @return lluminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        return Post::findOrFail($id);
    }

    /**
     * Insert or Update Post
     * @param $request
     * @param lluminate\Database\Eloquent\Model
     * @return void 
     */
    public function save($request, $postMl){
        $postMl->name = $request->name;
        $postMl->slug = Str::slug($request->slug, '-');
        $postMl->content = $request->content;
        $postMl->type = $request->type;
        if ($request->hasFile('banner')) {
            $file = $request->file('banner');

            //Delete old file
            if (Storage::disk('public')->exists('banners/', $postMl->banner)) {
                Storage::disk('public')->delete("banners/$postMl->banner");
            }
            //path: banners/image.jpg
            $path = $file->storeAs(
                'public/banners', $file->getClientOriginalName()
            );
            $postMl->banner = $file->getClientOriginalName();
        }
        $postMl->save();
    }

    /**
     * Delete post
     * @param $id
     * @return bool
     */
    public function delete($id){
        $postMl = Post::find($id)->delete();
        return true;
    }

    /**
     * Change Status
     * @param $id
     * @return bool
     */
    public function changeStatus($id){
        $post = Post::find($id);
        if($post->status == 1)
            $post->status = 0;
        else 
            $post->status = 1;
        $post->save();
        return $post->status;
    }
}
