<?php 
namespace Modules\Posts\Repositories\Interfaces;

interface PostInterface
{
    public function find($id);
    public function getDatatable();
    public function save($request, $postMl);
    public function delete($id);

}
