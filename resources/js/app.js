require('./bootstrap');

window.Vue = require('vue');
import router from './router';
import store from './store';

import App from './App.vue';

import BootstrapVue from 'bootstrap-vue';
import FlashMessage from '@smartweb/vue-flash-message';

Vue.use(BootstrapVue);
Vue.use(FlashMessage);

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h=>h(App)
});
