import Vue from 'vue';
import Router from 'vue-router';
import Welcome from './views/Welcome.vue';
import Categories from './views/Categories.vue';
import Products from './views/Products.vue';
import Notfound from './views/404.vue';
import Register from './views/auth/Register.vue';
import Login from './views/auth/Login.vue';
import * as auth from './services/auth_service';
Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'welcome',
        component: () => import('./views/Welcome.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('./views/auth/Register.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./views/auth/Login.vue'),
        beforeEnter(to, from, next){
            if(!auth.isLoggedIn()){
                next();
            }
            else{
                next('/');
            }
        }
    },
    {
        path: '/categories',
        name: 'categories',
        component: () => import('./views/Categories.vue'),
        beforeEnter(to, from, next){
            if(auth.getUserRole() == "administrator"){
                next();
            }
            else{
                next('/404');
            }
        }
    },
    {
        path: '/products',
        name: 'products',
        component: () => import('./views/Products.vue'),
        beforeEnter(to, from, next){
            if(auth.getUserRole() == "author"){       //auth.isLoggedIn()
                next();
            }
            else{
                next('/404');
            }
        }
    },
    {
        path: '*',
        name: '404',
        component: () => import('./views/404.vue'),
    },
];

const router = new Router({
    // mode: 'history',
    routes: routes,
    linkActiveClass: 'active'
});
export default router;