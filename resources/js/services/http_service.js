import axios from 'axios';
import store from '../store';
import * as auth from './auth_service';

export function http(){
    return axios.create({
        baseURL: store.state.apiURL,  // trả về 'http://localhost/global/public/api'
        headers: {
            Authorization: 'Bearer ' + auth.getAccessToken()
        }
    });
}

export function httpFile(){
    return axios.create({
        baseURL: store.state.apiURL,
        headers:{
            'Content-Type': 'multipart/form-data',
            Authorization: 'Bearer ' + auth.getAccessToken()
        }
    });
}
