<h1>Notification for Order Shipped</h1>
<p>Thank you: {{ $customer->name }}</p>
<p>Product: {{ $order->name }}</p>
<p>Price: {{ $order->price }}</p>