<div class="table-actions">
    @if (!empty($edit))
        <a href="{{ route($edit, $item->id) }}" class="btn btn-icon btn-sm btn-primary tip"
           data-original-title="{{ trans('action.edit') }}"><i class="fa fa-edit"></i></a>
    @endif

    @if (!empty($delete))
        <button data-toggle="modal" class="btn btn-sm btn-danger" data-target="#confirmModal" onclick="deleteURL('{{ route($delete, $item->id) }}')">
            <i class="fa fa-trash"></i>
        </button>
    @endif
</div>
<script>
    function deleteURL(url) {
        $('#btnConfirmDelete').val(url);
    }
</script>
