@if(!@empty($badge))
    <a href="{{ $badge['link'] }}"><span class="badge {{ $badge['class'] }}">{{ $badge['text'] }}</span></a>
@endif

<script>
$("a[href='{{$badge['link']}}']").on('click', function(e){
    e.preventDefault();
    var el = $(this);
    $.ajax({
        url: el.attr('href'),
        method: 'POST',
        data: {
            "_token" : "{{ csrf_token() }}",
        },
        success: (data) => {
            let badge = el.children().first();
            if(data.status==1){
                badge.removeClass('badge-secondary').addClass('badge-success');
                badge.text("{{ trans('action.yes') }}");
            }
            else{
                badge.removeClass('badge-success').addClass('badge-secondary');
                badge.text("{{ trans('action.no') }}");
            }
        },
    });
});
</script>