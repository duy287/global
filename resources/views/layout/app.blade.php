<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
        <!--Datatables-->
        <link rel="stylesheet" type="text/css" href="{{ asset('lib/datatables/css/dataTables.bootstrap4.min.css') }}"/>
        <!-- IziToast -->
        <link rel="stylesheet" href="{{ asset('lib/iziToast/iziToast.min.css') }}">
        <!--Font-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        @include('layout.header')
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                @include('layout.sidebar')
            </div>
            <div id="layoutSidenav_content">
                @yield('content')
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/demo/chart-area-demo.js') }}"></script>
        <script src="{{ asset('assets/demo/chart-bar-demo.js') }}"></script>
        <!--Datatable-->
        <script src="{{ asset('lib/datatables/js/jquery.dataTables.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('lib/datatables/js/dataTables.bootstrap4.min.js') }}" crossorigin="anonymous"></script>
        <!-- IziToast -->
        <script src="{{ asset('lib/iziToast/iziToast.min.js') }}"></script>
        <script type="text/javascript">
            @if (Session::has('flash_message'))
                let level = "{{ Session::get('flash_level') }}";
                let message = "{{ Session::get('flash_message') }}";
                switch (level) {
                    case 'success':
                        iziToast.success({
                            title: 'Message',
                            message: message,
                            position: 'topRight'
                        });
                        break;
                    case 'warning':
                        iziToast.warning({
                            title: 'Warning',
                            message: message,
                            position: 'topRight'
                        });
                        break;
                    case 'error':
                        iziToast.error({
                            title: 'Error',
                            message: message,
                            position: 'topRight',
                        });
                        break;
                    default:
                        break;
                }
            @endif
        </script>
        @stack('page-scripts')
    </body>
</html>
