@extends('layout.app')

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">News-add</h1>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Form</div>
            <div class="card-body">
                <form method="POST" action="{{ route('news.update', $news->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group @error('title') has-danger @enderror">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="{{ old('title', $news->title) }}" class="form-control">
                        @error('title')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" name="content" rows="3">{{ old('content', $news->content) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="banner">Image</label>
                        <input type="file" name="image"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection