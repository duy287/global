@extends('layout.app')

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Example</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Confirm dialog-->
<div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <strong>{{__('action.delete_confirm')}}</strong>
            </div>
            <div id="confirmMessage" class="modal-body">
                {{__('action.delete_sure')}}
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmCancel" class="btn btn-default btn-cancel"
                        data-dismiss="modal">
                    {{__('action.cancel')}}
                </button>
                <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteItem($(this).val())">
                    {{__('action.delete')}}
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('news.index') }}",
        columns: [
            { data: 'title', name: 'title' },
            { data: 'content', name: 'content' },
            { data: 'image', name: 'image', orderable: false, searchable: false},
        ],
        columnDefs: [{
            "targets": 3, 
            "data": null,
            "render": function (data) {
                edit_href = data.edit_url;
                delete_event = ' onclick = checkItemExisted(' + data.id + ') ';
                return '<a class="btn btn-primary btn-sm" href=' + edit_href + '><span class="fa fa-edit"></span></a> ' +
                '<button class="btn btn-sm btn-danger" ' + delete_event + 'id="row_' + data.id + '" data-toggle="modal" data-target="#confirmDeleteItem" >' +
                    '<span class="fa fa-trash" ></span></button> ';
            }
        }],
    });

    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        const URL =  "http://localhost/global/public/news/";
        let request = $.ajax({
            url: URL + id,
            method: "DELETE",
            data: {
                "_token" : "{{ csrf_token() }}"
            }
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#row_${data.row_id}`).closest("tr").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }
</script>
@endpush