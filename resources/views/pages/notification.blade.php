@extends('layout.app')
@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('mark-notification', $user->id) }}" class="btn btn-success">Đã xem</a>
                <ul class="list-group">
                    @foreach ($user->unreadNotifications as $item)
                    <li class="list-group-item">
                        {{ json_encode($item->data) }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</main>
@endsection
