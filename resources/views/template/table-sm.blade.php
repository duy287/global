<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        body {
            margin: 20px;
            font-size: 14px;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            line-height: 1.4;
        }
    </style>
</head>
<body>
    <div class="container">
        <form>
            <div class="form-group">
                <label for="image">Image</label>
                <input type="file" class="form-control" id="image">
            </div>
            <button type="submit" class="btn btn-primary" onclick="">Submit</button>
        </form>
    </div>
    <table class="table table-bordered table-striped my-tb">                
        <tbody>
            @for($i=1; $i<=10; $i++)
            <tr>
                <td>{{ $i }}</td>
                <td>Name</td>
                <td>Email</td>
                <td>Created At</td>
                <td>Updated At</td>
            </tr>
            @endfor
        </tbody>
    </table>
</body>
</html>