<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
php artisan make:controller Api/PosttController --api
php artisan make:resource Post
php artisan make:resource PostCollection
*/
Route::apiResource('posts', 'Api\PostController');

/**
 * API Authentication
 */
Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'Api\UserController@details');
});

//===============================================================

/*
npm install vuex --save
npm install bootstrap-vue
npm i @smartweb/vue-flash-message
npm install jsonwebtoken
*/

Route::group(['prefix'=>'auth'], function(){
    Route::post('register', 'Api\AuthController@register');
    Route::post('login', 'Api\AuthController@login');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('logout', 'Api\AuthController@logout');
        Route::get('profile', 'Api\AuthController@profile');
    });    
});

//Quyền truy cập
Route::group(['prefix'=>'user', 'middleware'=> 'auth:api'], function(){
    Route::post('edit-category', function(){
        return response()->json([
            'message' => 'Admin access',
            'status_code' => 200
        ], 200);
    })->middleware('scope:do_anything');

    Route::post('create-category', function(){
        return response()->json([
            'message' => 'Everyone access',
            'status_code' => 200
        ], 200);
    })->middleware('scope:do_anything,can_create');

    Route::group(['middleware' => 'scope:can_create'], function(){
        Route::get('/user-scope', function(){
            return response()->json([
                'message'=> 'User can access this',
                'status_code'=>200
            ], 200);
        });
    });
    Route::group(['middleware' => 'scope:do_anything'], function(){
        Route::get('/admin-scope', function(){
            return response()->json([
                'message'=> 'Admin can access this',
                'status_code'=>200
            ], 200);
        });
    });
});

//Products
Route::group(['middleware'=> 'auth:api'], function(){
    Route::group(['middleware' => 'scope:can_create'], function(){
        Route::get('/get-categories','Api\ProductController@categories');
        Route::apiResource('products', 'Api\ProductController');
    });
    Route::group(['middleware' => 'scope:do_anything'], function(){
        Route::apiResource('/categories', 'Api\CategoryController');
    });   
});