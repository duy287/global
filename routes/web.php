<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{any}', function () {
//     return View('welcome');
// })->where('any', '.*');

Route::get('/', function(){
    return View('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//========================== EMAIL ===================================

//send mail
Route::get('/order/{id}', 'OrderController@ship')->name('mail');

//Review Mail tempalte
Route::get('/mailable', function () {
    $order = App\Order::find(1);
    return new App\Mail\OrderShipped($order);
});

//Send Mail via Queue
Route::get('/queue/{id}', 'OrderController@queueShip')->name('queue');

//Send Mail Notification
Route::get('/notification/{id}', 'OrderController@sendNotification')->name('notification');


//======================= DATABASE Notifications ===================
//Gửi notification (id=order_id)
Route::get('/db-notification/{id}', 'OrderController@sendDatabaseNotification')->name('db-notification');

//Đánh dấu các notification đã đọc của user (id=user_id)
Route::get('/mark-notification/{id}', 'OrderController@markNotification')->name('mark-notification');

//========================== Repositories ============================//
Route::resource('/news', 'NewsController');


//========================== Template ============================//
Route::get('/table-sm', function(){
    return view('template.table-sm');
});